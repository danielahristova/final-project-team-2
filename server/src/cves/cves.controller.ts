import { Controller, Get, Param, Body, Post, UseGuards, Delete, Patch } from '@nestjs/common';
import { CVEsService } from './cves.service';
import { CVEdto } from './models/cve-dto';
import { AuthGuard } from '@nestjs/passport';
import { SchedulerDTO } from './models/scheduler-dto';
import { AdminGuard } from '../common/guards/admin.guard';

@Controller('cves')
export class CVEsController {

    constructor(
        private cveService: CVEsService
    ) { }

    @Get('/all')
    async getAllCves() {
        return await this.cveService.getAllCves();
    }

    @Get(':cpe')
    @UseGuards(AuthGuard())
    async getCPE(@Param('cpe') CPE: string) {
        return await this.cveService.loadCVEs(CPE);
    }

    @Get('/local/:cpe')
    @UseGuards(AuthGuard())
    async getLocalCVEs(@Param('cpe') CPE: string) {
        return await this.cveService.loadLocalCVEs(CPE);
    }


    @Post(':cpe')
    @UseGuards(AuthGuard())
    async addCVEs(@Param('cpe') CPE: string, @Body() cve: CVEdto) {
        return await this.cveService.saveCVE(CPE, cve)
    }

    @Get('search/:id')
    async getById(@Param('id') id: string) {
        return await this.cveService.getById(id);
    }

    @Get('/update/status')
    @UseGuards(AuthGuard(), AdminGuard)
    async checkIfSetUp() {

        return await this.cveService.checkIfSetUp();
    }

    @Patch('/update')
    @UseGuards(AuthGuard(), AdminGuard)
    async activateUpdateCVE(@Body() data: SchedulerDTO) {
        return await this.cveService.addCronJob(data);
    }

    @Delete('/update')
    @UseGuards(AuthGuard(), AdminGuard)
    async deactivateUpdateCVE() {
        return await this.cveService.deleteCron();
    }
}
