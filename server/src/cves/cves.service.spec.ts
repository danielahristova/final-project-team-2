import { CVEsService } from "./cves.service";
import { CVERepository } from "../repositories/cve.repository";
import { CPERepository } from "../repositories/cpe.repository";
import { InventoryRepository } from "../repositories/inventory.repository";
import { TestingModule, Test } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { HttpService } from "@nestjs/common";
import { SchedulerRegistry } from "@nestjs/schedule";
import { CVEdto } from "./models/cve-dto";
import { CPEs } from "../database/entities/CPEs.entity";

describe('CVEsService', () => {

    let service: CVEsService;
    let http: any;
    let cpesRepo: any;
    let cvesRepo: any;
    let inventoryRepo: any;
    let scheduler: any;

    beforeEach(async () => {
        http = {
            get() { return null; },
        };

        cpesRepo = {
            findOne() { return null; },
        };

        cvesRepo = {
            find() { return null; },
            createQueryBuilder: jest.fn(() => ({
                leftJoinAndSelect: jest.fn().mockReturnThis(),
                where: jest.fn().mockReturnThis(),
                getMany: jest.fn().mockReturnThis()
            })),
            updateCVE() { return null; },
            saveCVE() { return null; },
        };

        inventoryRepo = {
            findOne() { return null; },
            setVulnerable() { return null; },
        };

        scheduler = {
            deleteCronJob() { return null; },
            getCronJobs() { return null; },
        };

        const module: TestingModule = await Test.createTestingModule({
            providers: [
                CVEsService,
                { provide: getRepositoryToken(CVERepository), useValue: cvesRepo },
                { provide: getRepositoryToken(CPERepository), useValue: cpesRepo },
                { provide: getRepositoryToken(InventoryRepository), useValue: inventoryRepo },
                { provide: HttpService, useValue: http },
                { provide: SchedulerRegistry, useValue: scheduler }
            ],
        }).compile();

        cvesRepo = module.get<CVERepository>(CVERepository);
        cpesRepo = module.get<CPERepository>(CPERepository);
        inventoryRepo = module.get<InventoryRepository>(InventoryRepository);
        http = module.get<HttpService>(HttpService);
        scheduler = module.get<SchedulerRegistry>(SchedulerRegistry);
        service = module.get<CVEsService>(CVEsService);
        jest.resetAllMocks();
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getAllCves', () => {
        it('calls cveRepo.find once', async () => {
            //Act
            const spy = jest.spyOn(cvesRepo, 'find');
            //Arrange
            await service.getAllCves();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns the result from cveRepo.find', async () => {
            //Act
            jest.spyOn(cvesRepo, 'find')
                .mockResolvedValue('test');
            //Arrange
            const result = await service.getAllCves();
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getById', () => {
        it('calls http.get once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(http, 'get');
            const id = 'someId';
            const url = `https://services.nvd.nist.gov/rest/json/cve/1.0/${id}`
            //Arrange
            service.getById(id);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });
    });

    describe('loadCVEs', () => {
        it('calls http.get once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(http, 'get');
            const cpe = 'cpe:2.3:a:pulsesecure:pulse_connect_secure:8.1'
            const cpePart = cpe.split(':').slice(3, 6).join(':');
            const url = `https://services.nvd.nist.gov/rest/json/cves/1.0?cpeMatchString=cpe:/a:${cpePart}&startIndex=0&resultsPerPage=20`;
            //Arrange
            service.loadCVEs(cpe);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });
    });

    describe('loadLocalCVEs', () => {
        it('calls cveRepo.createQueryBuilder once', async () => {
            //Act
            const spy = jest.spyOn(cvesRepo, 'createQueryBuilder');
            const cpe = `someCPE`;
            //Arrange
            service.loadLocalCVEs(cpe);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('saveCVE', () => {
        it('calls cpesRepo.findOne once with correct filter', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'findOne');
            const cpe = `someCPE`;
            const filter = { name: cpe };
            const cve: CVEdto = {
                id: 'id',
                description: 'desc',
                lastModifiedDate: 'never',
                severity: 'none',
                cpe: [new CPEs()]
            }
            //Arrange
            service.saveCVE(cpe, cve);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(filter);
        });
    });

    describe('deleteCron', () => {
        it('calls scheduler.deleteCronJob once', async () => {
            //Act
            const spy = jest.spyOn(scheduler, 'deleteCronJob');
            //Arrange
            service.deleteCron();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('checkIfSetUp', () => {
        it('calls scheduler.getCronJobs once', async () => {
            //Act
            const spy = jest.spyOn(scheduler, 'getCronJobs')
                .mockReturnValue('test');
            //Arrange
            await service.checkIfSetUp();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns true if getCronJobs result is greater than 0', async () => {
            //Act
            jest.spyOn(scheduler, 'getCronJobs')
                .mockReturnValue(new Map().set('test', 'test'));
            //Arrange
            const result = await service.checkIfSetUp();
            //Assert
            expect(result).toBeTruthy();
        });

        it('returns false if getCronJobs result is less than or equal to 0', async () => {
            //Act
            jest.spyOn(scheduler, 'getCronJobs')
                .mockReturnValue(new Map());
            //Arrange
            const result = await service.checkIfSetUp();
            //Assert
            expect(result).toBeFalsy();
        });
    });

    describe('manualUpdate', () => {
        it('calls http.get once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(http, 'get');
            const today = new Date().toISOString().slice(0, 10);
            const url = `https://services.nvd.nist.gov/rest/json/cves/1.0?modStartDate=${today}T00:00:00:000%20UTC-04:00&startIndex=0&resultsPerPage=500`
            //Arrange
            service.manualUpdate();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });
    });
});
