export class SchedulerDTO {
    name: string;
    hour: string;
    minutes: string;
}