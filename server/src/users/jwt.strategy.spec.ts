import { Test } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { UserRepository } from '../repositories/user.repository';
import { User } from '../database/entities/users.entity';

const mockUserRepository = () => ({
    findOne: jest.fn()
});

describe('JwtStrategy', () => {

    let jwtStrategy;
    let userRepository;

    beforeEach(async () => {
        process.env.JWT_SECRET = 'GoneIn86400Seconds'
        const module = await Test.createTestingModule({
            providers: [
                JwtStrategy,
                { provide: UserRepository, useFactory: mockUserRepository },
            ],     
        }).compile();
        userRepository = await module.get<UserRepository>(UserRepository);
        jwtStrategy = await module.get<JwtStrategy>(JwtStrategy);
    });

    describe('validate', () => {
        it('returns the user if JWT payload is correct', async () => {
            // Arrange
            const user = new User;
            userRepository.findOne.mockResolvedValue(user);
            // Act
            const result = await jwtStrategy.validate({ nickname: 'Pesho' });
            // Assert
            expect(result).toEqual(user);
        });
    });
});