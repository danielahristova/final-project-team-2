import { Controller, Get, Query, Post, Body, Put, Param, Delete, Logger, UseGuards, ParseIntPipe } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklisted-tokens.guard';
import { UsersService } from './users.service';
import { ShowUserDto } from './dto/showUserDto';
import { User } from '../database/entities/users.entity';
import { LoginUserDto } from './dto/loginUserDto';
import { FindUserDto } from './dto/findUserDto';
import { Token } from '../common/decorators/get-token.decorator';
import { GetUser } from '../common/decorators/get-user.decorator';
import { UpdateUserDto } from './dto/updateUserDto';
import { CreateUserDto } from './dto/createUserDto';

@Controller('users')
export class UsersController {
    private logger = new Logger('UsersController');

    constructor(private readonly userService: UsersService) { }

    @Get()
    @UseGuards(AuthGuardWithBlacklisting)
    async all(
        @Query() query: FindUserDto,
    ): Promise<ShowUserDto[]> {

        if (query){
            return await this.userService.find(query);
        }

        return await this.userService.all();
    }

    @Post('/register')
    async register(
        @Body() createUserDTO: CreateUserDto
    ): Promise<ShowUserDto> {
        return this.userService.register(createUserDTO);
    }

    @Post('/signin')
    async signIn(
        @Body() loginUserDTO: LoginUserDto
    ): Promise<{ accessToken: string }> {
        this.logger.verbose(`User with nickname \x1b[1m\x1b[4m\x1b[37m${ loginUserDTO.email }\x1b[0m\x1b[1m\x1b[36m signed in.\x1b[0m`);
        return this.userService.signIn(loginUserDTO);
    }

    @Delete('/signout')
    @UseGuards(AuthGuardWithBlacklisting)
    async signOut(
        @Token() token: string): Promise<string> {
        return this.userService.signOut(token);
    }

    @Put(':id')
    @UseGuards(AuthGuardWithBlacklisting)
    async update(
        @Body() body: UpdateUserDto,
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User
    ): Promise<ShowUserDto> {
        return await this.userService.update(id, body, user);
    }
}
