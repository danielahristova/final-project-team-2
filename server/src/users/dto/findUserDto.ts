import { IsOptional, IsNotEmpty, IsEmail } from "class-validator";

export class FindUserDto {
    @IsOptional()
    @IsNotEmpty()
    search: string;

    @IsOptional()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsOptional()
    @IsNotEmpty()
    username: string;
}
