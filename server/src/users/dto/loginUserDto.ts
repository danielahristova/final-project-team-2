import { IsNotEmpty, IsString, IsEmail } from "class-validator";

export class LoginUserDto {
@IsString()
@IsNotEmpty()
password: string;

@IsEmail()
@IsNotEmpty()
email: string;
}