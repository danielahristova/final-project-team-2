import { IsEmail, IsString, MinLength, MaxLength, Matches, IsOptional } from "class-validator";

export class UpdateUserDto {
    @IsEmail()
    @IsOptional()
    email: string;

    @IsString()
    @MinLength(2)
    @MaxLength(15)
    @IsOptional()
    firstName: string;

    @IsString()
    @MinLength(2)
    @MaxLength(15)
    @IsOptional()
    lastName: string;

    @IsString()
    @IsOptional()
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        { message: 'Password must include capital letters, lowercase letters, numbers and special characters' })
    password: string;

    // @IsOptional()
    // @IsString()
    // avatarUrl: string;
}