import { IsString, IsEmail, MinLength, MaxLength, Matches } from "class-validator";

export class CreateUserDto {
    @IsEmail()
    @MinLength(10)
    @MaxLength(60)
    email: string;

    @IsString()
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        { message: 'Password must include capital letters, lowercase letters, numbers and special characters'})
    password: string;

    @IsString()
    @MinLength(4)
    @MaxLength(35)
    firstName: string;

    @IsString()
    @MinLength(4)
    @MaxLength(35)
    lastName: string;

    @IsString()
    @MinLength(4)
    @MaxLength(25)
    username: string;

}