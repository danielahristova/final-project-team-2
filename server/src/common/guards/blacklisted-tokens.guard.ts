import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { UsersService } from "../../users/users.service";

@Injectable()
export class AuthGuardWithBlacklisting extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly userService: UsersService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!(await super.canActivate(context))) {
      return false;
    }

    const request: Request = context.switchToHttp().getRequest();
    // Check if the token in the request is blacklisted
    const token: string = (request.headers as any).authorization;
    if (this.userService.isTokenBlacklisted(token)) {
      return false;
    }
    return true;
  }
}