import { Injectable, HttpService } from "@nestjs/common";
import { CPERepository } from "../repositories/cpe.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { InventoryRepository } from '../repositories/inventory.repository';
import { CpeDto } from './dto/cpeDto';
import * as fs from 'fs';
import * as readline from 'readline';
import { Stream } from 'stream';
import * as gunzip from 'gunzip-file'
import '../common/string.extensions'
import { SchedulerDTO } from "../cves/models/scheduler-dto";
import { SchedulerRegistry } from "@nestjs/schedule";
import { SystemError } from "../common/filters/system.error";
import * as cron from 'cron'

@Injectable()
export class CPEsService {

    constructor(
        private httpService: HttpService,
        @InjectRepository(CPERepository)
        private cpeRepo: CPERepository,
        @InjectRepository(InventoryRepository)
        private inventoryRepo: InventoryRepository,
        private readonly schedulerRegistry: SchedulerRegistry
    ) { }

    async initialLoadCPEsAPI() {
        let i = 0;

        let allCPEs = 0;
        do {
            console.time()
            const request = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cpes/1.0?startIndex=${i}&resultsPerPage=5000`).toPromise();
            allCPEs = request.data.totalResults;
            const cpesArray = request.data.result.cpes
            await this.saveCPEs(cpesArray);
            i += 5000;
            console.timeEnd()
        } while (i <= allCPEs + 5000);
    }

    async addCronJob(data: SchedulerDTO) {
        const { name, hour, minutes } = data;

        const job = new cron.CronJob(`${minutes} ${hour} * * *`, async () => {
            const today = new Date().toISOString().slice(0, 10);

            try {
                const update = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cpes/1.0?modStartDate=${today}T00:00:00:000%20UTC-05:00&startIndex=0&resultsPerPage=500`).toPromise();
                const cpesArray = await update.data.result.cpes;
                this.saveCPEs(cpesArray);

            } catch{
                throw new SystemError(`Did not receive a timely response from the upstream server.`, 504);
            }
        });

        this.schedulerRegistry.addCronJob(name, job);
        job.start();
    }

    async deleteCron() {
        this.schedulerRegistry.deleteCronJob('updateCPEs');
    }

    async saveCPEs(cpesArray) {
        for (const cpe of cpesArray) {
            const name = cpe.cpe23Uri;
            if (name[8] == 'a') {
                await this.cpeRepo.saveCPE(name);
            }
        }
    }

    async getAllRelevantCPEs(vendor: string, productName: string): Promise<CpeDto[]> {
        return await this.cpeRepo.getRelevantCPEs(vendor, productName)
    }

    async getSuggestedCpes(inventoryItemId: number): Promise<CpeDto[]> {
        const foundProduct = await this.inventoryRepo.findOne({ id: inventoryItemId });
        const vendor = foundProduct.vendor.replace(/\s/g, "_").toLowerCase();
        const product = foundProduct.product.replace(/\s/g, "_").toLowerCase();
        const version = foundProduct.version;
        const cpeArray = await this.getAllRelevantCPEs(vendor, product);
        return this.cpeRepo.suggestedCpes(cpeArray, vendor, product, version);
    }

    private async downloadCPEs() {
        const zippedFileName = 'official-cpe-dictionary_v2.3.xml.gz';
        const writer = fs.createWriteStream(zippedFileName);
        const response = await this.httpService.axiosRef({
            url: 'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.gz',
            method: 'GET',
            responseType: 'stream',
        });
        await response.data.pipe(writer);

        return new Promise((resolve, reject) => {
            writer.on('finish', resolve);
            writer.on('error', reject);
        });
    }

    private async xmlParser(inputFile: string, outputCpes: string) {
        const cpes = []
        const instream = fs.createReadStream(inputFile);
        const outstream = new Stream();
        const rl = readline.createInterface(instream, outstream as any);
        // let counter = 0
        cpes.push(`id,cpeUri,type,vendor,product,version,update,edition,language`)
        rl.on(`line`, line => {
            if (line.includes('<cpe-23:cpe23-item')) {
                const specialCharactersObject = { '[,]': '%2c', '[(]': '%28', '[)]': '%29', '[|]': '%7c' }
                const cpeUri = `"${line.substring(29, line.length - 3)}",`;
                const cpeUriArray = cpeUri.split(':');
                const type = `"${cpeUriArray[2]}",`;
                const vendor = `"${cpeUriArray[3].replace(/\\/g, '')}",`;
                const product = `"${cpeUriArray[4].replace(/\\/g, '')}",`;
                const version = `"${cpeUriArray[5].replace(/\\/g, '')}",`;
                const update = `"${cpeUriArray[6].replace(/\\/g, '')}",`;
                const edition = `"${cpeUriArray[7].replace(/\\/g, '')}",`;
                const language = `"${cpeUriArray[8].replace(/\\/g, '')}",`;
                const webName = `"cpe:2.3:${cpeUriArray[2]}:${cpeUriArray[3]}:${cpeUriArray[4]}:${cpeUriArray[5]}"`.specialCharactersReplace(specialCharactersObject)
                if (type === '"a",') {
                    cpes.push('\n' + cpeUri + type + vendor + product + version + update + edition + language + webName)
                }
            }
        });
        instream.on('close', () => {
            fs.writeFile(outputCpes, cpes, () => {
                this.cpeRepo.cpeQuery();
                fs.unlink(`${inputFile}.gz`, (err) => {
                    if (err) throw err;
                });
                fs.unlink(inputFile, (err) => {
                    if (err) throw err;
                });
            });
        });
    }

    private async handleZippedFile(zippedFile: string) {
        const unzippedFile = zippedFile.substring(0, zippedFile.length - 3);
        await gunzip(zippedFile, unzippedFile, async () => {
            await this.xmlParser(unzippedFile, 'cpes.csv');
        })
    }

    async initialLoadCPEsFile() {
        console.time()
        await this.downloadCPEs()
        await this.handleZippedFile('official-cpe-dictionary_v2.3.xml.gz');
        console.timeEnd()
    }

    async checkIfEmpty(): Promise<boolean> {
        const foundRow = await this.cpeRepo.count();
        if (foundRow > 0) return false;
        else return true;
    }

    async getAllProductNames() {
        return await this.cpeRepo.getAllProductNames();
    }

    async getAllVendorNames() {
        return await this.cpeRepo.getAllVendorNames();
    }

    async checkIfProductExists(productName: string): Promise<number> {
        return this.cpeRepo.checkIfProductExists(productName);
    }

    async checkIfVendorExists(vendorName: string): Promise<number> {
        return this.cpeRepo.checkIfVendorExists(vendorName);
    }

    async checkIfCpeExists(vendorName: string, productName: string, version: string): Promise<CpeDto> {
        return this.cpeRepo.checkIfCpeExists(vendorName, productName, version);
    }
}