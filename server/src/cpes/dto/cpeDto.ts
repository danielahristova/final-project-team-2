import { Expose } from 'class-transformer'

export class CpeDto {

    @Expose()
    name: string;

    @Expose()
    type: string;

    @Expose()
    vendor: string;

    @Expose()
    product: string;

    @Expose()
    version: string;

    @Expose()
    update: string;

    @Expose()
    edition: string;

    @Expose()
    language: string;
}