import { Expose } from 'class-transformer'

export class ShowCpeDto {

    @Expose()
    type: string;

    @Expose()
    vendor: string;

    @Expose()
    product: string;

    @Expose()
    version: string;

    @Expose()
    update: string;

    @Expose()
    edition: string;

    @Expose()
    language: string;
}