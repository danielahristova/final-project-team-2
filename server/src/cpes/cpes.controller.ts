import { Controller, Get, Param, ParseIntPipe, UseGuards, Delete, Patch, Body } from "@nestjs/common";
import { CPEsService } from "./cpes.service";
import { ShowCpeDto } from './dto/showCpeDto';
import { AuthGuard } from "@nestjs/passport";
import { AdminGuard } from "../common/guards/admin.guard";
import { CpeDto } from './dto/cpeDto';
import { SchedulerDTO } from "../cves/models/scheduler-dto";

@Controller('cpes')
export class CPEsController {

   constructor(private readonly cpeService: CPEsService) { }

   @Get('/api')
   @UseGuards(AuthGuard(), AdminGuard)
   async getCPEApi() {
      return await this.cpeService.initialLoadCPEsAPI()
   }

   @Get('/file')
   @UseGuards(AuthGuard(), AdminGuard)
   async getCPEFile() {
      return await this.cpeService.initialLoadCPEsFile()
   }

   @Get('/products')
   async getAllProductNames() {
      return await this.cpeService.getAllProductNames()
   }

   @Get('/vendors')
   async getAllVendorNames() {
      return await this.cpeService.getAllVendorNames()
   }

   @Get('/product/:productName')
   async checkIfProductExists(
      @Param('productName') productName: string,
   ): Promise<number> {
      return await this.cpeService.checkIfProductExists(productName)
   }

   @Get('/vendor/:vendorName')
   async checkIfVendorExists(
      @Param('vendorName') vendorName: string,
   ): Promise<number> {
      return await this.cpeService.checkIfVendorExists(vendorName)
   }

   @Get('/inventory/:inventoryItemId/data')
   @UseGuards(AuthGuard())
   async getSuggestedCpes(
      @Param('inventoryItemId', ParseIntPipe) inventoryItemId: number,
   ): Promise<ShowCpeDto[]> {

      return await this.cpeService.getSuggestedCpes(inventoryItemId);
   }

   @Get('/:vendorName/:productName/:version')
   async checkIfCpeExists(
      @Param('vendorName') vendorName: string,
      @Param('productName') productName: string,
      @Param('version') version: string,
   ): Promise<CpeDto> {
      return await this.cpeService.checkIfCpeExists(vendorName, productName, version)
   }

   @Get()
   @UseGuards(AuthGuard())
   async checkIfEmpty(): Promise<boolean> {

      return await this.cpeService.checkIfEmpty();
   }

   @Patch('/update')
   @UseGuards(AuthGuard(), AdminGuard)
   async activateUpdateCPE(@Body() data: SchedulerDTO) {
      return await this.cpeService.addCronJob(data);
   }

   @Delete('/update')
   @UseGuards(AuthGuard(), AdminGuard)
   async deactivateUpdateCPE() {
      return await this.cpeService.deleteCron();
   }

}