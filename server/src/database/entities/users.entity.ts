import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, Unique, BaseEntity } from 'typeorm';
import { UserRole } from '../../common/enums/user-roles';
import * as bcrypt from 'bcrypt';

@Entity('users')
@Unique(['username'])
@Unique(['email'])

export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 15 })
  firstName: string;

  @Column('nvarchar', { length: 15 })
  lastName: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column('nvarchar', { length: 30 })
  email: string;

  @Column({
    type: "enum",
    enum: UserRole,
    default: UserRole.Basic
  })
  role: UserRole;

  // @Column({ nullable: true })
  // avatarUrl: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  createdDate: Date;

  @Column()
  salt: string;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

}