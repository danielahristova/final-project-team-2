import { Inventory } from "../database/entities/inventory.entity";
import { EntityRepository, Repository } from "typeorm";


@EntityRepository(Inventory)
export class InventoryRepository extends Repository<Inventory> {


    async setVulnerable(inventory: Inventory) {
        inventory.vulnerable = true;
        this.save(inventory);
    }
}