import { Repository, EntityRepository } from "typeorm";
import { User } from '../database/entities/users.entity';
import { CreateUserDto } from "../users/dto/createUserDto";
import { InternalServerErrorException, Logger } from "@nestjs/common";
import * as bcrypt from 'bcrypt';
import { LoginUserDto } from "../users/dto/loginUserDto";
import { SystemError } from "../common/filters/system.error";
import { FindUserDto } from "../users/dto/findUserDto";
import { ShowUserDto } from '../users/dto/showUserDto';
import { plainToClass } from 'class-transformer';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    private logger = new Logger('UserRepository');

    async register(createUserDto: CreateUserDto): Promise<ShowUserDto> {
        const {email, password, firstName, lastName, username } = createUserDto;

        const user = new User();
        user.email = email;
        user.salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(password, user.salt);
        user.firstName = firstName;
        user.lastName = lastName;
        user.username = username;
        
        try {
            await user.save();
            return plainToClass(ShowUserDto, user, { excludeExtraneousValues: true });
        } catch (error) {
            if ( error.code === 'ER_DUP_ENTRY' && error.message.includes(user.email)) {
                this.logger.error(`Failed to create user \x1b[1m\x1b[4m\x1b[37m${ user.username }\x1b[0m\x1b[31m because of duplicate email entry.`);
                throw new SystemError('There is a user with this email', 409)
            }else if (error.code === 'ER_DUP_ENTRY' && error.message.includes(user.username)) {
                this.logger.error(`Failed to create user \x1b[1m\x1b[4m\x1b[37m${ user.username }\x1b[0m\x1b[31m because of duplicate username entry.`);
                throw new SystemError('Username taken', 409)
            }
            this.logger.error(`Failed to create user \x1b[1m\x1b[4m\x1b[37m${ user.username }\x1b[0m\x1b[31m.`, error.stack);
            throw new InternalServerErrorException();
        }  
    } 

    async validateUserPassword(loginUserDto: LoginUserDto): Promise<User> {
        const {password, email } = loginUserDto;

        const user = await this.findOne({ email });
        if (user && await user.validatePassword(password)) {
            return user;
        }
        return null;
    }

    async getUsers(options: FindUserDto): Promise<User[]> {
        const { search , username , email } = options;
            const query = this.createQueryBuilder('users');
            
        if(username){
            query.where('users.username = :username', { username });
        }
        if(email){
            query.andWhere('users.email = :email', { email });
        }
        if (search) {
            query.andWhere('users.username LIKE :search OR users.email LIKE :search', { search: `%${search}%` })
        }
        query.having('users.isDeleted = :isDeleted', { isDeleted: false });
        const users = await query.getMany();
        
        return users
    }

    private async hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }
}