import { Test } from '@nestjs/testing';
import { UserRepository } from './user.repository';
import * as bcrypt from 'bcrypt';
import { SystemError } from "../common/filters/system.error"
import { User } from '../database/entities/users.entity';

const mockCreateUserDto = { 
    email: 'PI@abv.bg',
    password: 'asd123!@',
    firstName: 'Pesho',
    lastName: 'Ivanov',
    nickname: 'Peshkata',
};

describe('UserRepository', () => {

    let userRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                UserRepository,
            ],     
        }).compile();
        userRepository = await module.get<UserRepository>(UserRepository);
    });

    describe('register', () => {
        let save;
        beforeEach(() => {
            save = jest.fn();
            userRepository.create = jest.fn().mockReturnValue({ save });
        })

        it('creates new user', async () => {
            // Arrange
            save.mockResolvedValue('created');
            // Act, Assert
            expect(userRepository.register(mockCreateUserDto)).resolves.not.toThrow();
        });

        it('throws Conflict exception if user already exists', async () => {
            // Arrange;
            save.mockRejectedValue('ER_DUP_ENTRY');
            // Act, Assert
            expect(userRepository.register(mockCreateUserDto)).rejects.toThrow(SystemError);
        });

        it('throws Conflict exception if user already exists', async () => {
            // Arrange;
            save.mockRejectedValue('problem');
            // Act, Assert
            expect(userRepository.register(mockCreateUserDto)).rejects.toThrow(SystemError);
        });
    });

    describe('validateUserPassword', () => {
        let user;
        beforeEach(() => {
            userRepository.findOne = jest.fn();
            user = new User;
            user.nickname = 'Peshkata';
            user.validatePassword = jest.fn();
        })

        it('returns the user if successful', async () => {
            // Arrange
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(true);
            // Act
            const result = await userRepository.validateUserPassword(mockCreateUserDto);
            // Assert
            expect(result).toEqual(user);
        });

        it('throws Conflict exception if user already exists', async () => {
            // Arrange
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(false);
            // Act
            const result = await userRepository.validateUserPassword(mockCreateUserDto);
            // Assert
            expect(result).toEqual(null);
        });

        it('returns null if username doesn\'t exist', async () => {
            // Arrange;
            userRepository.findOne.mockResolvedValue(null);
            // Act
            const result = await userRepository.validateUserPassword(mockCreateUserDto);
            // Assert
            expect(result).toEqual(null);
        });
    });

    describe('hashPassword', () => {

        it('calls bcrypt.hash in order to hash password', async () => {
            // Arrange
            const spy = jest.spyOn(bcrypt, 'hash');
            spy.mockResolvedValue('hash')
            // Act
            const result = await userRepository.hashPassword('password', 10)
            // Assert
            expect(result).toEqual('hash');
        });
    });
});