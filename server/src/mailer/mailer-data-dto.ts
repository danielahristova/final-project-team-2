export class MailerDataDTO {
    user: string;
    cpe: string;
    cves: string[];
}