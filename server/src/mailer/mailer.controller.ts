import { Controller, Body, Post } from '@nestjs/common';
import { MailerService } from './mailer.service';
import { MailerDataDTO } from './mailer-data-dto';

@Controller()
export class MailerController {

    constructor(private readonly mailerService: MailerService) { }

    @Post('/sendMail')
    async sendMail(
        @Body() data: MailerDataDTO
    ): Promise<void> {
        return await this.mailerService.sendMail(data);
    }
}
