import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../repositories/user.repository';
import { sendEmail } from './sendEmail';
import { MailerDataDTO } from './mailer-data-dto';

@Injectable()
export class MailerService {

    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
    ) { }

    async sendMail(data: MailerDataDTO): Promise<void> {
        const admin = await this.userRepository.findOne({ where: { role: 'Admin' } });

        const email = admin.email;

        const { user, cpe, cves } = data
        await sendEmail(email, this.emaiBody(user, cpe, cves))
    }

    emaiBody(user: string, cpe: string, cves: string[]) {
        return `${user} assigned to ${cpe} the following CVE(s):\n${cves}`
    }

}
