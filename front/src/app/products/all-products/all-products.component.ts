import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { LoginService } from '../../login/login.service';
import { InventoryItemDto } from '../../models/inventory-items';
import { DeleteProductComponent } from '../delete-product/delete-product.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})
export class AllProductsComponent implements OnInit {

  products = [];
  showLoader: boolean;
  userName: string;

  constructor(
    private productsService: ProductsService,
    private loginService: LoginService,
    public dialog: MatDialog,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.userName = this.loginService.loggedUser().nickname;
    this.getAllProducts();
    this.productsService.isAllProductsUsed = true;
  }

  getAllProducts() {
    this.showLoader = true;
    const getAllProductsObserver = {
      next: x => { this.products = x; this.showLoader = false; },
      error: err => console.error(err)
    };
    this.productsService.getAllProducts().subscribe(getAllProductsObserver);
  }

  openDeleteProductDialog(productId: number): void {
    const dialogRef = this.dialog.open(DeleteProductComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        productId,
        url: this.router.url
      }
    });
  }

  closeDeleteProductDialog(): void{
    this.dialog.closeAll();
  }

}
