import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { LoginService } from '../../login/login.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeleteProductComponent } from '../delete-product/delete-product.component';

@Component({
  selector: 'app-assigned-products',
  templateUrl: './assigned-products.component.html',
  styleUrls: ['./assigned-products.component.scss']
})
export class AssignedProductsComponent implements OnInit {

  products = [];
  showLoader: boolean;
  userName: string;

  constructor(
    private productsService: ProductsService,
    private loginService: LoginService,
    public dialog: MatDialog,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.userName = this.loginService.loggedUser().nickname;
    this.getAssignedProducts();
    this.productsService.isAllProductsUsed = false;
  }

  getAssignedProducts() {
    this.showLoader = true;
    const getAssignedProductsObserver = {
      next: x => { this.products = x.filter(product => product.cpe !== null && product.cves.length !== 0); this.showLoader = false; },
      error: err => console.error(err)
    };
    this.productsService.getAllProducts().subscribe(getAssignedProductsObserver);
  }

  openDeleteProductDialog(productId: number): void {
    const dialogRef = this.dialog.open(DeleteProductComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        productId,
        url: this.router.url
      }
    });
  }

  closeDeleteProductDialog(): void{
    this.dialog.closeAll();
  }

}
