import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { CVEDto } from '../../models/CVEs';
import { LoginService } from '../../login/login.service';
import { CveService } from '../cve.service';

@Component({
  selector: 'app-single-cve',
  templateUrl: './single-cve.component.html',
  styleUrls: ['./single-cve.component.scss']
})
export class SingleCveComponent implements OnInit {

  cves: CVEDto[];
  userName: string;
  showLoader: boolean;

  constructor(
    private cveService: CveService,
    private loginService: LoginService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public cveId: string,
  ) { }

  ngOnInit(): void {
    this.userName = this.loginService.loggedUser().userName;
    this.getSingleCve();
  }

  getSingleCve() {
    this.showLoader = true;
    const getSingleCveObserver = {
      next: x => {
        this.cves = x;
        this.showLoader = false;
      },
      error: err => console.error(err)
    };
    this.cveService.getSingleCve(this.cveId).subscribe(getSingleCveObserver);
  }

  closeDeletePostModal(): void{
    this.dialog.closeAll();
  }
}

