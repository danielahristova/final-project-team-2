import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllProductsComponent } from './all-products/all-products.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { CreateNewComponent } from './create-new/create-new.component';
import { AuthGuard } from '../login/auth-guard/auth.guard';
import { AssignedProductsComponent } from './assigned-products/assigned-products.component';
import { UnassignedProductsComponent } from './unassigned-products/unassigned-products.component';


const routes: Routes = [
  { path: '', redirectTo: 'all', pathMatch: 'full' },
  { path: 'all', canActivate: [AuthGuard], component: AllProductsComponent },
  { path: 'new', canActivate: [AuthGuard], component: CreateNewComponent },
  { path: 'assigned-products', canActivate: [AuthGuard], component: AssignedProductsComponent },
  { path: 'unassigned-products', canActivate: [AuthGuard], component: UnassignedProductsComponent },
  { path: ':id', canActivate: [AuthGuard], component: SingleProductComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
