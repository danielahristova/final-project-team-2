import { Component, OnInit } from '@angular/core';
import { CpeDto } from '../../models/CPEs';
import { InventoryItemDto } from '../../models/inventory-items';
import { ProductsService } from '../products.service';
import { LoginService } from '../../login/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { SingleCveComponent } from '../single-cve/single-cve.component';
import { CVEDto } from '../../models/CVEs';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { CpeService } from '../cpe.service';
import { CveService } from '../cve.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit {

  clickedProduct;
  isProductClicked = false;

  cpes = [];
  clickedCpe: CpeDto;
  isCpeClicked = false;
  cancelButtonShow = false;

  cves = [];
  cvesToSave = [];
  cvesToShowInSingle = [];
  oldCves = [];
  newCves = [];
  updateClicked = false;

  userName: string;
  hasCpe = 'default';
  checked = false;
  showLoader: boolean;

  constructor(
    private productsService: ProductsService,
    private cpeService: CpeService,
    private cveService: CveService,
    private loginService: LoginService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.userName = this.loginService.loggedUser().username;
    this.getProductById();
  }

  getProductById() {
    this.showLoader = true;
    const getProductByIdObserver = {
      next: x => {
        this.clickedProduct = x;
        this.showLoader = false;
        this.checkIfProductHasCpe(this.clickedProduct.cpe);
        if (this.hasCpe === 'none') {
          this.getSuggestedCpes(this.clickedProduct.id);
        } else {
          this.getCvesFromLocalDb(this.clickedProduct.cpe.name);
        }
        this.hideNotClicked(this.clickedProduct.id);
      },
      error: err => console.error(err)
    };
    this.productsService.getProductById(+this.route.snapshot.paramMap.get('id')).subscribe(getProductByIdObserver);
  }

  checkIfProductHasCpe(productCpe: null | CpeDto) {
    if (productCpe === null) {
      this.hasCpe = 'none';
    } else {
      this.hasCpe = 'exists';
    }
  }

  hideNotClicked(identifier: number | string) {
    if (typeof identifier === 'number') {
      this.isProductClicked = true;
      if (this.hasCpe === 'exists') {
        this.cancelButtonShow = false;
        this.cpes = [];
      } else {
        this.cancelButtonShow = true;
      }
    } else {
      this.clickedCpe = this.cpes.find(cpe => cpe.name === identifier);
      this.isCpeClicked = true;
      this.cancelButtonShow = false;
    }
  }

  getSuggestedCpes(inventoryItemId: number) {
    this.showLoader = true;
    const getSuggestedCpesObserver = {
      next: x => { this.cpes = x; this.showLoader = false; },
      error: err => console.error(err)
    };
    this.cpeService.getSuggestedCpes(inventoryItemId).subscribe(getSuggestedCpesObserver);
  }

  close() {
    if (this.productsService.isAllProductsUsed === true) {
      this.router.navigateByUrl('/products/all');
      this.productsService.isAllProductsUsed = false;
    } else {
      this.goBack();
      this.isProductClicked = false;
      this.cpes = [];
      this.updateClicked = false;
    }
  }

  goBack() {
    this.location.back();
  }

  linkCpeToProduct(inventoryItemId: number, cpeName: string) {
    this.showLoader = true;
    const linkCpeToProductObserver = {
      next: x => { this.successSnackBar('Linked'); this.showLoader = false; },
      error: err => {
        console.error(err),
          this.errorSnackBar();
      }
    };
    this.productsService.linkCpeToProduct(inventoryItemId, cpeName).subscribe(linkCpeToProductObserver);
  }

  successSnackBar(message: string) {
    this.snackBar.open(message, 'End', {
      duration: 1500,
      panelClass: ['green-snackbar']
    });
  }

  errorSnackBar() {
    this.snackBar.open('Error', 'End', {
      duration: 1500,
      panelClass: ['red-snackbar']
    });
  }

  breakLinkToCpe(inventoryItemId: number) {
    this.showLoader = true;
    const breaklinkCpeToProductObserver = {
      next: x => {
        this.successSnackBar('Link to CPE removed'),
          this.getProductById(),
          this.showLoader = false;
        this.isProductClicked = false;
      },
      error: err => {
        console.error(err),
          this.errorSnackBar();
      }
    };
    this.productsService.breakLinkToCpe(inventoryItemId).subscribe(breaklinkCpeToProductObserver);
  }

  getCvesForCpe(cpeName: string) {
    this.showLoader = true;
    const getCvesForCpeObserver = {
      next: x => { this.cves = x; this.showLoader = false; },
      error: err => console.error(err)
    };
    this.cveService.getCvesForCpe(cpeName).subscribe(getCvesForCpeObserver);
  }

  openSingleCveDialog(cveId: string): void {
    this.dialog.open(SingleCveComponent, {
      panelClass: 'custom-dialog-container',
      data: cveId
    });
  }

  checkboxClicked(event, cve: CVEDto) {
    if (event.checked === true) {
      this.cvesToSave.push(cve);
    } else {
      const indexOfCve = this.cvesToSave.findIndex(el => el.id === cve.id);
      this.cvesToSave.splice(indexOfCve, 1);
    }
  }

  linkCveToProduct(cpeName: string) {
    this.showLoader = true;
    const linkCvesToProductObserver = {
      next: x => {
        this.successSnackBar('Linked'),
        this.getProductById(),
        this.showLoader = false;
        this.isCpeClicked = false;
        this.updateClicked = false;
        this.cvesToSave = [];
        this.getCvesFromLocalDb(cpeName);
      },
      error: err => console.error(err)
    };
    this.cvesToSave.map(cve => this.cveService.linkCveToProduct(cpeName, cve).subscribe(linkCvesToProductObserver));
    this.productsService.alert(this.userName, cpeName, this.cvesToSave).subscribe();
  }

  getCvesFromLocalDb(cpeName: string) {
    this.showLoader = true;
    const getCvesFromLocalDbObserver = {
      next: x => { this.cvesToShowInSingle = x; this.showLoader = false; },
      error: err => console.error(err)
    };
    this.cveService.getCvesFromLocalDb(cpeName).subscribe(getCvesFromLocalDbObserver);
  }

  async updateCvesInSingle(cpeName: string) {
    this.showLoader = true;
    this.cves = [];
    this.oldCves = [];
    this.newCves = [];
    this.updateClicked = true;
    const getCvesForCpeObserver = {
      next: x => {
        this.cves = x;
        this.cves.map(cve => {
          if (this.cvesToShowInSingle.find(oldCve => oldCve.id === cve.id)) {
            this.oldCves.push(cve);
          } else {
            this.newCves.push(cve);
          }
        });
        this.showLoader = false;
      },
      error: err => console.error(err)
    };
    this.cveService.getCvesForCpe(cpeName).subscribe(getCvesForCpeObserver);
  }

}
