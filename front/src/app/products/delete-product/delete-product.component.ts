import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.scss']
})
export class DeleteProductComponent implements OnInit {

  constructor(
    private readonly productService: ProductsService,
    public dialog: MatDialog,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: { productId: number, url: string }
  ) { }

  ngOnInit(): void {

  }

  deleteProduct(): void{
    const deleteProductObserver = {
      next: x => {
        this.closeProductDialog();
        if (this.data.url === '/products/all') {
          this.router.navigate([`${this.data.url}`]);
        } else {
          this.router.navigate(['/search']);
        }
      },
      error: err => console.error(err)
    };
    this.productService.deleteProduct(this.data.productId).subscribe(deleteProductObserver);
  }

  closeProductDialog(): void{
    this.dialog.closeAll();
  }
}
