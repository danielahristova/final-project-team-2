import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { LoginService } from '../../login/login.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeleteProductComponent } from '../delete-product/delete-product.component';

@Component({
  selector: 'app-unassigned-products',
  templateUrl: './unassigned-products.component.html',
  styleUrls: ['./unassigned-products.component.scss']
})
export class UnassignedProductsComponent implements OnInit {

  products = [];
  showLoader: boolean;
  userName: string;

  constructor(
    private productsService: ProductsService,
    private loginService: LoginService,
    public dialog: MatDialog,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.userName = this.loginService.loggedUser().nickname;
    this.getUnassignedProducts();
    this.productsService.isAllProductsUsed = false;
  }

  getUnassignedProducts() {
    this.showLoader = true;
    const getUnassignedProductsObserver = {
      next: x => { this.products = x.filter(product => product.cpe === null || product.cves.length === 0); this.showLoader = false; },
      error: err => console.error(err)
    };
    this.productsService.getAllProducts().subscribe(getUnassignedProductsObserver);
  }

  openDeleteProductDialog(productId: number): void {
    const dialogRef = this.dialog.open(DeleteProductComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        productId,
        url: this.router.url
      }
    });
  }

  closeDeleteProductDialog(): void{
    this.dialog.closeAll();
  }

}
