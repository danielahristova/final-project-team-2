import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import trie from 'trie-prefix-tree';
import { CpeService } from './products/cpe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Xposed';
  productNamesLoaded = false;
  showLoader: boolean;

  constructor(
    private router: Router,
    private cpeService: CpeService,
  ) {
    this.getAllProductNames();
    this.getAllVendorNames();
    this.router.events.subscribe((RouterEvent => {
      if (RouterEvent instanceof NavigationStart) {
        this.showLoader = true;
      }
      if (RouterEvent instanceof NavigationEnd ||
        RouterEvent instanceof NavigationCancel ||
        RouterEvent instanceof NavigationError) {
        this.showLoader = false;
      }
    }));
  }

  getAllProductNames() {
    const getAllProductNamesObserver = {
      next: x => {
        const data = x.map(value => value.product);
        this.cpeService.productNames = trie(data);
        this.cpeService.productNamesRaw = data;
        this.productNamesLoaded = true;
      },
      error: err => console.error(err)
    };
    this.cpeService.getAllProductNames().subscribe(getAllProductNamesObserver);
  }

  getAllVendorNames() {
    const getAllVendorsNamesObserver = {
      next: x => {
        const data = x.map(value => value.vendor);
        this.cpeService.vendorsNames = trie(data);
        this.cpeService.vendorsNamesRaw = data;
      },
      error: err => console.error(err)
    };
    this.cpeService.getAllVendorsNames().subscribe(getAllVendorsNamesObserver);
  }

}
