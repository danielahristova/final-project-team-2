import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { CVEDto } from '../models/CVEs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProductsService } from '../products/products.service';
import { InventoryItemDto } from '../models/inventory-items';
import { CveService } from '../products/cve.service';


@Injectable({
  providedIn: 'root'
})
export class ResultsResolverService implements Resolve<CVEDto | InventoryItemDto[] | string>{

  constructor(
    private readonly productService: ProductsService,
    private readonly cveService: CveService,
  ) { }
  resolve(route: ActivatedRouteSnapshot): Observable<CVEDto | InventoryItemDto[] | string> {

    if (route.queryParams.search) {
      const search = route.queryParams.search;
      return this.cveService.getCVEbyID(search)
        .pipe(
          catchError((err: string) => of(err))
        );
    } else if (route.queryParams.keyword) {
      const keyword = route.queryParams.keyword;
      return this.productService.getQuery(keyword)
        .pipe(
          catchError((err: string) => of(err))
        );
    } else {
      return null;
    }
  }
}
