import { Component, OnInit, OnDestroy } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { CVEDto } from '../models/CVEs';
import { Subscription } from 'rxjs';
import { LoginService } from '../login/login.service';
import { InventoryItemDto } from '../models/inventory-items';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'app-search-result',
    styleUrls: ['search-result.component.scss'],
    templateUrl: 'search-result.component.html',
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class SearchResultComponent implements OnInit, OnDestroy {

    private loggedInSubscription: Subscription;
    loggedIn: boolean;
    admin: boolean;
    searchText: string;
    result: MatTableDataSource<CVEDto | InventoryItemDto[] | string>;
    CVEs: boolean;
    Products: boolean;
    columnsToDisplayCVE = ['id', 'lastModifiedDate', 'severity'];
    columnsToDisplayProducts = ['product', 'vendor', 'version', 'cpe'];
    expandedElement: CVEDto | CVEDto[] | null;

    error: string;

    constructor(
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly authservice: LoginService,

    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {

            if (typeof data.result === 'string' ||
                data.result === null) {
                this.CVEs = false;
                this.Products = false;
                this.error = data.result;
            }
            else if (!!data.result[0].hasOwnProperty('severity')) {
                this.CVEs = true,
                    this.Products = false;
                this.error = null;
                this.result = data.result;
            } else {
                this.CVEs = false;
                this.Products = true;
                this.error = null;
                this.result = data.result;
            }
        });

        this.loggedInSubscription = this.authservice.isLoggedIn$.subscribe(
            loggedIn => this.loggedIn = loggedIn,
        );
    }

    ngOnDestroy(): void {
        this.loggedInSubscription.unsubscribe();
    }

    async search() {
        if (!this.loggedIn) {
            this.router.navigate(
                [],
                {
                    relativeTo: this.activatedRoute,
                    queryParams: { search: this.searchText }
                });
        } else {
            this.router.navigate(
                [],
                {
                    relativeTo: this.activatedRoute,
                    queryParams: { keyword: this.searchText }
                });
        }
    }
    productSelected(id: number) {
        return this.router.navigate([`products/${id}`]);
    }
}
