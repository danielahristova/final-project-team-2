import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent implements OnInit, OnDestroy {

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;
  public loggedIn: boolean;
  user: any;


  constructor(
    public dialog: MatDialog,
    private readonly router: Router,
    private readonly authservice: LoginService,
  ) { }

  ngOnDestroy(): void {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
  ngOnInit(): void {
    this.loggedInSubscription = this.authservice.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn
    );
    this.userSubscription = this.authservice.loggedUser$.subscribe(
      user => this.user = user
    );
  }
  logOut(): void {
    this.authservice.logout();
  }

  logIn(): void {
    this.router.navigate(['login']);
  }
}
