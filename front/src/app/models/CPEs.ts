import { CVEDto } from './CVEs';

export class CpeDto {
    name: string;
    type: string;
    vendor: string;
    product: string;
    version: string;
    update: string;
    edition: string;
    language: string;
    webName: string;
    // tslint:disable-next-line: variable-name
    __cve__: CVEDto[];
}
