import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  searchText = '';

  public loggedIn: boolean;
  constructor(public router: Router) { }

  search(){
    this.router.navigate(['search'], { queryParams: { search: this.searchText } });
  }

}
