import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminService } from './admin.service';

@Injectable({
    providedIn: 'root'
})
export class AdminResolverService implements Resolve<boolean | string>{

    constructor(
        private readonly adminService: AdminService,
    ) { }
    resolve(): Observable<boolean | string> {
        return this.adminService.checkIfEmptyCPE();
    }
}
